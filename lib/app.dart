import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterpaypalapi/src/apis/payments.dart';
import 'package:flutterpaypalapi/src/paypal_client.dart';
import 'package:flutterpaypalapi/src/paypal_rest_api.dart';
import 'package:http/http.dart';

class MyApp extends StatefulWidget {
  const MyApp();

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  PayPalRestApi payPalRestApi;

  @override
  void initState() {
    super.initState();
    payPalRestApi = PayPalRestApi(PayPalClient(IOClient(), "", ""));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Paypal Api'),
        ),
        body: Center(
          child: Text(
            'Hello world',
          ),
        ),
        floatingActionButton: FloatingActionButton(onPressed: (){
          payPalRestApi.payments.createPayment(Payment());
        },),
      ),
    );
  }
}
