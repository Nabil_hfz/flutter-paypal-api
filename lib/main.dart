
import 'package:flutter/material.dart';
import 'package:flutterpaypalapi/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}